#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import csv
import configparser
from pprint import pprint

# Read the config file
config = configparser.ConfigParser()
config.read('config.ini')

BASE_DIR = config.get('sympa', 'base_dir')
SERVER_NAME = config.get('sympa', 'server_name')
LISTS_FILE = config.get('sympa', 'lists_file')

lists = []
for dpath, dnames, fnames in os.walk(BASE_DIR):
    if 'blacklist.txt' in fnames:
        lists.append(dpath.split('/')[-2])

with open (LISTS_FILE, "w") as lists_file:
    for item in lists:
        lists_file.write("{}\n".format(item))
