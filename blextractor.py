#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import csv
import configparser
from pprint import pprint

# Read the config file
config = configparser.ConfigParser()
config.read('config.ini')

BASE_DIR = config.get('sympa', 'base_dir')
SERVER_NAME = config.get('sympa', 'server_name')
OUTPUT_FILE = config.get('sympa', 'output_file')

MAILING_LISTS = ['comm', 'paris']

output = []

for ml in MAILING_LISTS:
    with open (os.path.join(BASE_DIR, ml, 'search_filters/blacklist.txt'), 'r') as local_blacklist:
        entries = []
	for l in local_blacklist:
	    l = l.strip()
            if l not in entries:
		entries.append(l)
                output.append({'list': ml, 'entry': l})

pprint(output)
keys = ['list', 'entry']
with open(OUTPUT_FILE, 'wb') as output_file:
  writer = csv.DictWriter(output_file, keys)
  writer.writeheader()
  writer.writerows(output)
